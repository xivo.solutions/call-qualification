FROM node:16-alpine as build
WORKDIR /app

RUN npm install -g @angular/cli

COPY ./package.json .
COPY ./package-lock.json .
RUN npm ci
COPY . .
RUN npm run build

FROM nginx as runtime
COPY --from=build /app/dist/call-qualification-frontend /usr/share/nginx/html/

EXPOSE 80