import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { CallDataSubmit } from '@models/call-data-submit.model';
import { Observable, catchError, throwError } from 'rxjs';
import { QualificationViewModel } from '@models/qualification-view-model.model';


@Injectable({
  providedIn: 'root'
})
export class RequesterService {
  xucAddress: string

  constructor(private _http: HttpClient) {
    this.xucAddress = new URL(document.location.href).hostname || "MISSING_XUC_ADDRESS_CONFIG"
  }

  getQualifications(queueId: number, token: string): Observable<Array<QualificationViewModel>> {
    let params = new HttpParams().set('token', token);
    return this._http.get<Array<QualificationViewModel>>(`https://${this.xucAddress}/xuc/api/2.0/call_qualification/queue/${queueId}`, {
      params: params,
      headers: {
        'Access-Control-Allow-Origin': '*'
      }
    });
  }

  getAgentConfig(token: string): Observable<any> {
    let params = new HttpParams().set('token', token)
    return this._http.get(`https://${this.xucAddress}/xuc/api/2.0/agent`, {params: params})
  }

  getAgentQueues(token: string): Observable<any> {
    let params = new HttpParams().set('token', token)
    return this._http.get(`https://${this.xucAddress}/xuc/api/2.0/agent/queue_members`, {params: params})
  }

  getUserToken(username: string, password: string) {
    return this._http.post(`https://${this.xucAddress}/xuc/api/2.0/auth/login`, {"login": username, "password": password, "softwareType": "webBrowser"})
      .pipe(catchError((err) => {
        console.error(err)
        return throwError(() => new Error(`Impossible de se connecter au serveur - ${err.status}`));
      }))
  }

  renewUserToken(token: string) {
    return this._http.get(`https://${this.xucAddress}/xuc/api/2.0/auth/check`, {headers: new HttpHeaders({"Authorization": `Bearer ${token}`})})
      .pipe(catchError((err) => {
        console.error(err)
        return throwError(() => new Error(`Impossible de se connecter au serveur - ${err.status}`));
    }))
  }

  saveQualifications(submitData: CallDataSubmit, token: string): Observable<any> {
    let params = new HttpParams().set('token', token);
    return this._http.post(`https://${this.xucAddress}/xuc/api/2.0/call_qualification`, submitData , {
      params: params,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
      }
    });
  }

  getLineInUse(token: string): Observable<any> {
    let params = new HttpParams().set('token', token)
    return this._http.get(`https://${this.xucAddress}/xuc/api/2.0/agent/line`, {params: params})
      .pipe(catchError((err) => {
        console.error(err);
        return throwError(() => new Error(`Impossible de se connecter au serveur - ${err.status}`));
    }))
  }
}
