import { Injectable } from '@angular/core';
import { QualificationConfig, QualificationField } from '../models/qualification-config.model';
import { Observable, take } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'lodash';
import { QualificationViewModel } from '@models/qualification-view-model.model';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  defaultConfig!: QualificationConfig;

  getDefaultConfig() {
    this._http.get<QualificationConfig>('assets/default.config.json').pipe(take(1)).subscribe(elem => this.defaultConfig = elem);
  }

  constructor(private _http: HttpClient) {
    this.getDefaultConfig();
  }

  getConfig(qualifications: Array<QualificationViewModel>): QualificationConfig {

    let config = this.defaultConfig;
    config.fields.forEach(elem => {
      if (elem?.isQualification) {
        elem.lines[0].values = qualifications;
      }
    })
    return config;
  }
}
