import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { QualificationModule } from '@pages/qualification/qualification.module';
import { QualificationComponent } from '@pages/qualification/components/qualification/qualification.component';
import { LoginComponent } from '@pages/qualification/components/login/login.component';

const routes: Routes = [
  { path: 'qualification', component: QualificationComponent },
  { path: 'login', component: LoginComponent },
  { path: '**', redirectTo: "login"}
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    QualificationModule

  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [RouterModule]
})
export class AppModule {
}
