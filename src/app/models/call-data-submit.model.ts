export interface CallDataSubmit {
  sub_qualification_id: number;
  time: string | undefined | null;
  callid: string;
  agent: number;
  queue: number;
  first_name: string;
  last_name: string;
  comment: string;
  custom_data: string;
}
/**
case class CallQualificationAnswer(
    sub_qualification_id: Int,
    time: String,
    callid: String,
    agent: Int,
    queue: Int,
    firstName: String,
    lastName: String,
    comment: String,
    customData: String
)
 **/