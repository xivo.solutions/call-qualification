export interface SubQualificationViewModel {
  id: number;
  name: string;
}

export interface QualificationViewModel {
  id: number;
  name: string;
  subQualifications: Array<SubQualificationViewModel>;
}
