import { GridsterItem } from 'angular-gridster2';

export enum QualificationFieldType {
  textField = 0,
  textArea = 1,
  autoComplete = 2,
  submitArea = 3,
  logoArea = 4,
}

export interface QualificationField extends GridsterItem {
  isQualification: boolean;
  lines: Array<QualificationFieldLines>
}

export class QualificationFieldLines {

  title: string = '';
  subTitle?: string;
  values?: Array<any>;
  fieldType: QualificationFieldType = QualificationFieldType.textField;
  fieldValue: any;
  subFieldType: any;
  readOnly: boolean = false;
  formControlName: string = '';
  subFormControlName?: string;
  isMandatory: boolean = false;
}

export interface QualificationConfig {
  title: string;
  fields: Array<QualificationField>;
  isCustom: boolean;
  qualificationIndex: number;
  timeoutAfter:number;
  timeoutText:string;
  background: string;
  cardBackground: string;
}
