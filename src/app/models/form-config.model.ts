export interface FormConfig {
  isMandatory: boolean;
  isReadOnly: boolean;
  defaultValue?: any;
  formatter: (value: any) => {};
  valueSelector?: (value: any) => {};
}
