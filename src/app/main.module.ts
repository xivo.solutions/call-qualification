import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ConfigService } from '@services/config.service';
import { RequesterService } from '@services/requester.service';
import { GridsterModule } from 'angular-gridster2';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MatOptionModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { ComponentsModule } from '@components/components.module';
import { MatSnackBar } from '@angular/material/snack-bar';


@NgModule({
  declarations: [],
  providers: [
    ConfigService,
    RequesterService,
    DatePipe,
    MatSnackBar
  ],
  imports: [
    CommonModule,
    GridsterModule,
    HttpClientModule,
    FontAwesomeModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    NgxMatSelectSearchModule,
    BrowserAnimationsModule,
    ComponentsModule
  ],
  exports: [
    GridsterModule,
    HttpClientModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    NgxMatSelectSearchModule,
    MatOptionModule,
    MatSelectModule,
    BrowserAnimationsModule,
    MatAutocompleteModule,
    ComponentsModule
  ]
})
export class MainModule {
}
