import { Injectable } from '@angular/core';
import { DisplayGrid, GridsterConfig, GridsterItem } from 'angular-gridster2';

@Injectable({
  providedIn: 'root'
})
export class GridsterService {

  options!: GridsterConfig;
  readonlyOptions!: GridsterConfig;
  dashboard!: Array<GridsterItem>;

  constructor() {

    this.options = {
      maxCols: 20,
      maxRows: 20,
      minRows: 10,
      defaultItemCols: 1,
      defaultItemRows: 1,
      fixedRowHeight: 100,
      keepFixedHeightInMobile: false,
      keepFixedWidthInMobile: false,
      scrollSensitivity: 10,
      scrollSpeed: 20,
      enableEmptyCellClick: false,
      enableEmptyCellContextMenu: false,
      enableEmptyCellDrop: false,
      enableEmptyCellDrag: false,
      enableOccupiedCellDrop: false,
      emptyCellDragMaxCols: 50,
      emptyCellDragMaxRows: 50,
      ignoreMarginInRow: false,
      draggable: {
        enabled: true
      },
      resizable: {
        enabled: true
      },
      swap: false,
      pushItems: true,
      disablePushOnDrag: false,
      disablePushOnResize: false,
      pushDirections: {north: true, east: true, south: true, west: true},
      pushResizeItems: false,
      displayGrid: DisplayGrid.Always,
      disableWindowResize: false,
      disableWarnings: false,
      scrollToNewItems: false
    };
    this.readonlyOptions = {
      minCols: 4,
      minRows: 10,
      maxCols: 20,
      maxRows: 20,
      defaultItemCols: 1,
      defaultItemRows: 1,
      fixedRowHeight: 100,
      displayGrid: DisplayGrid.None,
    };

    this.dashboard = [
      {cols: 2, rows: 1, y: 0, x: 0},
      {cols: 2, rows: 2, y: 0, x: 2},
      {cols: 1, rows: 2, y: 2, x: 0}
    ];
  }
}
