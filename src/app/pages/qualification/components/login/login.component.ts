import {Component} from '@angular/core';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import {RequesterService} from '@services/requester.service';
import { Router } from '@angular/router';
import { CommonModule } from '@angular/common';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.less'],
    standalone: true,
    imports: [
        MatInputModule,
        MatFormFieldModule,
        CommonModule
    ]
})

export class LoginComponent {
    formError: string | undefined

    constructor(
        private _requesterService: RequesterService,
        private _router: Router
    ) {
        localStorage.clear()
    }

    login = (username: string, password: string) => {
        this._requesterService.getUserToken(username, password)?.subscribe({
        
        next: (res) => {
            let data = res as {TTL: string, token: string, login: string}
            localStorage.setItem("TTL", data.TTL)
            localStorage.setItem("token", data.token)
            localStorage.setItem("login", data.login)
            this._router.navigate(['/qualification'], {queryParams: { "standalone": "true" }} )
        },

        error: (err) => {
            this.formError = err
        }

        })
    }

}
