import {Component, OnInit, ViewChild} from '@angular/core';
import {RequesterService} from '@services/requester.service';
import {QualificationViewModel} from '@models/qualification-view-model.model';
import {ActivatedRoute, Params} from '@angular/router';
import {GridsterService} from '@pages/admin/services/gridster.service';
import {ConfigService} from '@services/config.service';
import {GridsterComponent} from 'angular-gridster2';
import {Observable, forkJoin} from 'rxjs';
import {QualificationConfig, QualificationFieldLines, QualificationFieldType} from '@models/qualification-config.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {isNil} from 'lodash';
import {CallDataSubmit} from '@models/call-data-submit.model';
import {DatePipe} from '@angular/common';
import {CountdownComponent, CountdownConfig, CountdownEvent} from 'ngx-countdown';
import { Router } from '@angular/router';
import { mergeMap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
    selector: 'app-qualification',
    templateUrl: './qualification.component.html',
    styleUrls: ['./qualification.component.less']
})
export class QualificationComponent implements OnInit {
    qualificationFieldType = QualificationFieldType;
    data!: any;
    qualifications: Array<QualificationViewModel> = [];
    agentNumber!: number;

    isNil(elem: any) {
        return isNil(elem);
    }

    getQualifications() {
        return this.qualifications;
    }

    setQualifications(qualificationViewModels: Array<QualificationViewModel>) {
        this.qualifications = qualificationViewModels.sort((a, b) => a.name.localeCompare(b.name));
        this.qualifications.forEach(qualification => {
            if (qualification.subQualifications) {
                qualification.subQualifications.sort((a, b) => a.name.localeCompare(b.name));
            }
        });

        this.config = this._configService.getConfig(this.qualifications);
        if (this.config.timeoutAfter > 0) {
            window.addEventListener("message", (event) => {
                if (event.data === 'xivo-StartCountDown') {
                    this.displayCountdown = true;
                    this.countdownConfig.leftTime = this.config.timeoutAfter;
                }
            })
        }
        this.buildFormGroup();
        if(this.data.standalone) {
            this.refreshAgentLine()
            setInterval(this.refreshAgentLine, 2 * 60000)
        }
    }

    refreshAgentLine = () => {
        this._requesterService.getLineInUse(localStorage.getItem('token') as string).subscribe({
            next: (res)  => this.processAgentLine(res),
            error: (err) => {
                this.formGroup.controls['callee'].patchValue("déconnecté")
                console.error(`Erreur lors de la récupération de la ligne agent: ${err}`)
            }
        })
    }

    renewToken = (token: string) => {
        this._requesterService.renewUserToken(token)?.subscribe({
            next: (res) => {
                let data = res as {TTL: string, token: string, login: string}
                localStorage.setItem("TTL", data.TTL)
                localStorage.setItem("token", data.token)
                localStorage.setItem("login", data.login)
                setTimeout(this.renewToken, (Number(localStorage.getItem('TTL') as string) / 2) * 1000, localStorage.getItem('token'))
            },

            error: (err) => {
                console.error(`Erreur lors du renouvellement du token: ${err}`)
                this._router.navigate(['login'])
            }
        });
    }

    $defaultConfig!: Observable<QualificationConfig>;
    config!: QualificationConfig;

    @ViewChild('admingrid', { static: true })
    grid!: GridsterComponent;
    formGroup!: FormGroup;


    get dashboard() {
        return this._gridsterService.dashboard;
    }

    get options() {
        return this._gridsterService.readonlyOptions;
    }

    constructor(
        private _requesterService: RequesterService,
        private _route: ActivatedRoute,
        private _gridsterService: GridsterService,
        private _configService: ConfigService,
        private _datePipe: DatePipe,
        private _router: Router,
        private _snackBar: MatSnackBar
    ) {
    }


    ngOnInit(): void {
        this._route.queryParams.subscribe((params: Params) => this.data = params);
        if (this.data.standalone) {
            this.prepareStandaloneQualification()
            setTimeout(this.renewToken, (Number(localStorage.getItem('TTL') as string) / 2) * 1000, localStorage.getItem('token'))
        }
        else {
            this._requesterService.getQualifications(this.data['queueId'][0], this.data['token'])
                .subscribe(qualifications => this.setQualifications(qualifications));
        }
    }

    getDataValue(propertyName?: string) {
        return propertyName && !isNil(this.data) ? this.data[propertyName] : '';
    }

    qualificationFormatter = (value: any) => value ? value.name : '';

    getSubQualifications(formControlName: string) {
        return this.formGroup.get(formControlName)?.value?.subQualifications;
    }

    getSubQualificationsControlName(line: QualificationFieldLines): string {
        return line?.subFormControlName ? line?.subFormControlName : ''
    }

    getQualificationsControlName(line: QualificationFieldLines): string {
        return line?.formControlName ? line?.formControlName : ''
    }

    onChange = () => {
        this.formGroup.get('subQualifications')?.setValue(null);
    }

    processAgentLine = (userline: {data: {number: number}}) => {
        console.log("Got agent line response", userline)
        this.agentNumber = userline?.data?.number
        if (this.agentNumber) this.formGroup.controls['callee'].patchValue(this.agentNumber)
        else this.formGroup.controls['callee'].patchValue("déconnecté")
    }

    prepareStandaloneQualification = () => {
        let token: string | null = localStorage.getItem('token')

        if (token !== null) {
            this.setupForAgent(token)
        } else this._router.navigate(['/login'])
    }

    setupForAgent = (token: string) => {
        this._requesterService.getAgentQueues(token).pipe(mergeMap((res: {data: [{agentId: number, queueId: number, penalty: number}]}) => {
            let qualificationRequest = res.data.map(queue => {
                return this._requesterService.getQualifications(queue.queueId, token as string)
            })

            return forkJoin(qualificationRequest)
        })).subscribe({
            next: (value: Array<Array<QualificationViewModel>>) => {
                this.setQualifications(value.flat())
            },
            error: (err) => {
                console.error(`Erreur lors de la récupération des qualifications: ${err}`);
            }
        })
    }


    private buildFormGroup() {
        this.formGroup = new FormGroup({});
        this.config.fields.forEach(l => {
            l.lines.forEach( field => {
                if (field.fieldType === QualificationFieldType.logoArea || field.fieldType === QualificationFieldType.submitArea) {
                    return
                }

                let formControl = new FormControl('');
                if (field.isMandatory) {
                    formControl.addValidators(Validators.required);
                }
                if (field.readOnly) {
                    formControl.disable();
                }
                if (field.fieldValue) {
                    formControl.setValue(this.getDataValue(field.fieldValue));
                }

                this.formGroup.addControl(field.formControlName, formControl);

                if (field.subFormControlName) {
                    formControl = new FormControl('');
                    if (field.isMandatory) {
                        formControl.addValidators(Validators.required);
                    }
                    if (field.readOnly) {
                        formControl.disable();
                    }
                    this.formGroup.addControl(field.subFormControlName, formControl);        
                }

            })
        })
    }

    onSaveForm() {
        this.formGroup.markAllAsTouched();
       
        if (this.formGroup.status !== 'VALID') {
            return
        }
        let format = "yyyy-MM-dd HH:mm:ss";
        let selectedQualificationId = this.formGroup.get('qualification')?.value?.id
        let selectedSubQualificationId = this.formGroup.get('subQualification')?.value?.id

        if (!selectedQualificationId) {
            this._snackBar.open("Erreur lors de la sauvegarde, champ qualification manquant", undefined, {
                duration: 4000
            })
            return
        } else {
            if (!selectedSubQualificationId) {
                this._snackBar.open("Erreur lors de la sauvegarde, champ sous-qualification manquant", undefined, {
                    duration: 4000
                })
                return
            }
            
        }

        let callDataSubmit: CallDataSubmit = {
            agent: this.data.standalone ? Number(this.agentNumber) :  Number(this.formGroup.get('callee')?.value),
            sub_qualification_id: selectedSubQualificationId,
            callid: this.data.standalone ? "0" : this.data['callId'],
            comment: this.formGroup.get('comment')?.value ? this.formGroup.get('comment')?.value : '',
            first_name: this.formGroup.get('firstName')?.value ? this.formGroup.get('firstName')?.value : '',
            last_name: this.formGroup.get('lastName')?.value ? this.formGroup.get('lastName')?.value : '',
            queue: this.data.standalone ? 0 : Number(this.data['queueId'][0]),
            custom_data: JSON.stringify({
                caller: this.formGroup.get('caller')?.value,
                contract: this.formGroup.get('contractNumber')?.value,
                optField1: this.formGroup.get('optField1')?.value,
                optField2: this.formGroup.get('optField2')?.value,
                optField3: this.formGroup.get('optField3')?.value
            }),
            time: this._datePipe.transform(Date.now(), format)
        };
        this._requesterService.saveQualifications(callDataSubmit, this.data['token'] || localStorage.getItem('token')).subscribe({
                next: () => {
                    if (this.data.standalone) {
                        this.formGroup.reset()
                        this.formGroup.controls['callee'].patchValue(this.agentNumber)
                        this._snackBar.open("Qualification sauvegardée", undefined, {
                            duration: 4000
                        })
                    } else this.close();
                },
                error: (err) => {
                    console.error(`Erreur lors de la sauvegarde de la qualification: ${err}`);
                    this._snackBar.open("Erreur lors de la sauvegarde", undefined, {
                            duration: 4000
                    })
                }
            })
    }

    close() {
        if (this.data.standalone) this._router.navigate(['/login'])
        else parent.window.postMessage("closeThirdParty", "*")
    }

    getCloseButtonLabel() {
        return this.data.standalone ? "se déconnecter" : "annuler"
    }

    @ViewChild('cd', {static: false}) countdown!: CountdownComponent;
    displayCountdown = false;
    countdownConfig: CountdownConfig = {
        demand: false,
        leftTime: 0,
        format: 'mm:ss',
        timezone: '+0000'
    }

    handleEvent($event: CountdownEvent) {
        if ($event.action === "done") {
            this.close();
        }
    }
}
