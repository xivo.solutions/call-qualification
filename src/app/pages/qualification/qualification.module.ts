import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QualificationComponent } from './components/qualification/qualification.component';
import { MainModule } from '../../main.module';
import { MatInputModule } from '@angular/material/input';
import {CountdownModule} from "ngx-countdown";

@NgModule({
  declarations: [
    QualificationComponent
  ],
  imports: [
    CommonModule,
    MainModule,
    MatInputModule,
    CountdownModule
  ]
})
export class QualificationModule {
}
