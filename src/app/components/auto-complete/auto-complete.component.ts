import { Component, Input, OnInit } from '@angular/core';
import { map, Observable, startWith } from 'rxjs';
import { isNil } from 'lodash';
import { ControlValueAccessor, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-auto-complete',
  templateUrl: './auto-complete.component.html',
  styleUrls: ['./auto-complete.component.less']
})
export class AutoCompleteComponent implements OnInit, ControlValueAccessor {

  @Input()
  set options(list: any[] | undefined) {
    this._options = list;
    this.resetFilter(true);
  }

  get options(): any[] | undefined {
    return this._options;
  }

  _options!: any[] | undefined;
  filteredOptions!: Observable<any[] | undefined> | undefined;


  @Input()
  onChange: any = () => {
  };

  @Input()
  onTouch: any = () => {
  };

  @Input()
  formatter = (value: any) => String(value);

  @Input()
  controlName!: string;

  @Input()
  form!: FormGroup;

  @Input()
  title: string | undefined = '';

  constructor() {
  }

  writeValue(obj: any): void {
    if (this.controlName) {
      this.form.get(this.controlName)?.setValue(obj);
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  resetFilter(resetValue: boolean) {
    if (resetValue && this.controlName) {
      this.form.get(this.controlName)?.setValue(null);
    }
    this.filteredOptions = this.form.get(this.controlName)?.valueChanges.pipe(
      startWith(''),
      map((value) => this._filter(value)),
    );
  }

  ngOnInit(): void {
    this.resetFilter(true);
  }


  private _filter(value: any): any[] | undefined {
    if (typeof value === 'string') {
      const filterValue = value.toLowerCase();
      if (!isNil(this.options)) {
        if (this.options.length === 1) {
          this.form.get(this.controlName)?.setValue(this.options[0]);
        }
        return this.options.filter(option => this.formatter(option).toLowerCase().includes(filterValue));
      }
    }
    return this.options;
  }

}
