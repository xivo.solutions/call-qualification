import { Component, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-textfield',
  templateUrl: './textfield.component.html',
  styleUrls: ['./textfield.component.less']
})
export class TextfieldComponent implements OnInit, ControlValueAccessor {

  @Input()
  defaultValue: any;

  @Input()
  onChange: any = () => {
  };

  @Input()
  onTouch: any = () => {
  };

  @Input()
  formatter = (value: any) => value;

  @Input()
  controlName!: string;

  @Input()
  form!: FormGroup;

  @Input()
  title: string | undefined = '';

  @Input()
  readonly: boolean = false;

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  writeValue(obj: any): void {
    if (this.controlName) {
      this.form.get(this.controlName)?.setValue(obj);
    }
  }

  constructor() {
  }

  ngOnInit(): void {
    if (this.defaultValue) {
      this.writeValue(this.defaultValue);
    }
  }

}
