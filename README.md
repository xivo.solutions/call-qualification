# Call Qualification Frontend

## Development server

to run locally your just have to run the commands
```BASH
npm install;
npm run start;
```

# Description

## What it is ?

The *Call Qualification* product (call qualification frontend and call qualification backend) is an application that can be used to qualify a call (during the call the agent answering the call can add information on the type of the call).

It uses the *Third Party Application* mechanism described here - see https://documentation.xivo.solutions/en/latest/api_sdk/third_party/index.html

## How does it work ?

### Call qualification in CCAgent

When configured :
1. The Call Qualification Backend is queried by the CC Agent - via the 3rd Party App `POST` mechanism (see https://documentation.xivo.solutions/en/latest/api_sdk/third_party/index.html#web-service-api)
   1. If it is a call towards a queue (i.e. the "queue" object is filled in the json object sent by the `POST` request) **and** some qualification are found for this queue (XiVO configuration)
   2. Then the *Call Qualification Backend* answers with
      1. `action` : *open*
      2. `event`: corresponds to the event type that triggers the qualification process. This value corresponds to the environment variable `EVENT` defined in your custom environment file. If the `EVENT` variable is not set, the default value will be `EventEstablished`, therefore the qualification opens when the agent answers the call.
      3. `autopause`: *true* if an autopause reason is set, *false* otherwise
      4. `autopauseReason`: the one defined in env var `AUTOPAUSE_REASON` or the one corresponding to the queue in env var `AUTOPAUSE_REASON_PER_QUEUE`
      5. `title`: *Qualification*
      6. `url`: an url pointing to the call qualification frontend
2. Then when the agent answers the call, the CC Agent will open the Qualification app (via the *Call Qualification Frontend*)
3. When the agent hangup he will be paused (selecting the pause given in the `AUTOPAUSE_REASON`) **WARN** behavior is undefined if this key is not defined with a correct pause type

Note:
- when installed the Qualification is triggered for every queue with qualifications attached to it
- the agent will be put in pause when the call is hangup
- the `AUTOPAUSE_REASON_PER_QUEUE` syntax is raw json -> `AUTOPAUSE_REASON_PER_QUEUE={"some-queue-A":"pause-for-A", "some-queue-B":"pause-for-B"}` where the key is the technical name of the queue and the value is the autopause wanted for the queue
- if both `AUTOPAUSE_REASON_PER_QUEUE` and `AUTOPAUSE_REASON` are unset in custom.env, then the API return will have `autopause` to false and `autopauseReason` will be undefined
- if both `AUTOPAUSE_REASON_PER_QUEUE` and `AUTOPAUSE_REASON` are set, then:
  - `AUTOPAUSE_REASON_PER_QUEUE` will be checked first
  - if the queue is missing from the json, then `AUTOPAUSE_REASON` will be applied


### Standalone call qualification

#### How does it work

Available on `/call-qualification/login` is the standalone login page.
When logged in, the line in use is retrieved and displayed in the "Agent" input.
The list of qualification corresponding to the queues of the user will be loaded too and added in the form dropdowns.
When saving the qualification, the form (except "Agent") is reset to empty and a small toast will notify that the qualification was saved.
If an error happens during save, then the form will stay filled and a small toast will notify that something went wrong.

Periodic API calls:
- The line in use by the agent is re-synchronized every two minutes to have the correct number in case of free-seatting agents.
- The token is renewed at half of the previous token TTL.

Limitations and things to improve:
- Most of the difference with the standard call qualification is based on a GET parameter `standalone=true`
- The queue and call-id in the POSTed form (and therefore in the DB at the end) will be set to 0
- The agent needs to be logged in on his CC Agent for the line in use sync to retrieve his number and identify himself in the db data

# Installation

XiVO
----
- deploy the dialplan queue subroutine from `xivo/call_qualification_queue_subroutine.conf` and do `dialplan reload`
on Asterisk CLI your can find it here => https://gitlab.com/xivo.solutions/call-qualification-backend/-/blob/master/xivo/call_qualification_queue_subroutine.conf?ref_type=heads

XiVO CC
-------

1. Create an override compose file that contains:

```yaml
services:
  nginx:
    links:
      - call-qualification-frontend
      - call-qualification-backend

    environment:
      - NGINX_CALL_QUALIF_FRONT_HOST=call-qualification-frontend
      - NGINX_CALL_QUALIF_FRONT_PORT=80
      - NGINX_CALL_QUALIF_BACK_HOST=call-qualification-backend
      - NGINX_CALL_QUALIF_BACK_PORT=9000

  call-qualification-frontend:
    image: xivoxc/call-qualification-frontend:2024.10.${XIVOCC_DIST}

  call-qualification-backend:
    image: xivoxc/call-qualification-backend:2024.10.${XIVOCC_DIST}

    links:
    - xuc

    restart: always

    volumes:
    - /etc/timezone:/etc/timezone:ro
    - /etc/localtime:/etc/localtime:ro
    - /var/log/xivocc:/opt/docker/var/log

    environment:
    - XUC_HOST
    - BACK_END_BASE_URL=https://${XUC_HOST}/qualification
    - FRONT_END_BASE_URL=https://${XUC_HOST}/call-qualification
    - AUTOPAUSE_REASON
    - AUTOPAUSE_REASON_PER_QUEUE
    - EVENT

    logging:
      driver: "json-file"
```


3. Add environment variables `AUTOPAUSE_REASON` and `THIRD_PARTY_URL` to the custom.env file:
- `THIRD_PARTY_URL` is the URL called by the client to fetch the call qualification configuration
- `AUTOPAUSE_REASON` is the pause that will be applied after hang up when the qualification is still opened. Must be a reason configured on the XiVO (technical name)
- `AUTOPAUSE_REASON_PER_QUEUE` (optional) is the JSON to specify the pause for one or multiple queue
- `EVENT` (optional) is the xuc phone event used to open the call qualification, EventEstablished by default

```sh
AUTOPAUSE_REASON=some-reason
THIRD_PARTY_URL=https://${XUC_HOST}/qualification/thirdparty/ws
```

Note: currently, the call-qualification-frontend and call-qualification-backend images are tagged for 2024.10.latestdev and these shall be used for maia, naos and so on.

## Template Nginx XiVOCC
This is to be set in a template file mounted inside nginx.

- Run `mkdir -p /etc/docker/nginx/templates/ && docker cp xivocc-nginx-1:/etc/nginx/templates/xivoxc.conf.template /etc/docker/nginx/templates/`
- Add volume `/etc/docker/nginx/templates/:/etc/nginx/templates/` to the volumes section in your yml file
- Add the following in `/etc/docker/nginx/templates/xivoxc.conf.template`, just before the last closing brace, then recreate nginx

```nginx

  # -------------------------------------
  # * call-qualification Front End ------
  # -------------------------------------
  location /call-qualification/ {
    proxy_pass http://${NGINX_CALL_QUALIF_FRONT_HOST}:${NGINX_CALL_QUALIF_FRONT_PORT}/;
  }

  location /call-qualification/qualification {
    proxy_pass http://${NGINX_CALL_QUALIF_FRONT_HOST}:${NGINX_CALL_QUALIF_FRONT_PORT}/;
  }

  location /call-qualification/login {
    proxy_pass http://${NGINX_CALL_QUALIF_FRONT_HOST}:${NGINX_CALL_QUALIF_FRONT_PORT}/;
  }

  location /call-qualification/assets/xivo_logo.svg {
    proxy_pass http://${NGINX_CALL_QUALIF_FRONT_HOST}:${NGINX_CALL_QUALIF_FRONT_PORT}/;
  }

  # -------------------------------------
  # * call-qualification Back End -------
  # -------------------------------------
  location /qualification/ {
    proxy_pass http://${NGINX_CALL_QUALIF_BACK_HOST}:${NGINX_CALL_QUALIF_BACK_PORT}/;
  }

```

## Template Nginx Edge
This is to be set in a template file mounted inside nginx (volume `/etc/docker/nginx/templates/:/etc/nginx/templates/`).

- Run `mkdir -p /etc/docker/nginx/templates/ && docker cp edge-nginx-1:/etc/nginx/templates/edge-proxy.conf.template /etc/docker/nginx/templates/`
- Add volume `/etc/docker/nginx/templates/:/etc/nginx/templates/` to the volumes section in your yml file
- Add the following in `/etc/docker/nginx/templates/edge-proxy.conf.template`, just before the last closing brace, then recreate nginx

```nginx
  # -------------------------------------
  # * call-qualification APIs -----------
  # -------------------------------------

  location ~ ^/xuc/api/2.0/(agent|user)/ {
    allow all;
    limit_req zone=apis burst=10;
    proxy_pass https://${XIVOCC_HOST};
    proxy_intercept_errors off;

    include /etc/nginx/xivo/proxy-ws_params;
    include /etc/nginx/xivo/proxy_params;
  }

  location ~ ^/xuc/api/2.0/agent {
    limit_req zone=apis burst=10;
    proxy_pass https://${XIVOCC_HOST};

    include /etc/nginx/xivo/proxy_params;
  }

  # -------------------------------------
  # * call-qualification Front End ------
  # -------------------------------------
  location /call-qualification {
    limit_req zone=webapp burst=20;
    proxy_pass https://${XIVOCC_HOST};

    include /etc/nginx/xivo/proxy_params;
  }

  # -------------------------------------
  # * call-qualification Back End ------
  # -------------------------------------
  location /qualification/thirdparty/ws {
    limit_req zone=webapp burst=20;
    proxy_pass https://${XIVOCC_HOST};

    include /etc/nginx/xivo/proxy_params;
  }

  location ~ ^/xuc/api/2.0/call_qualification {
    limit_req zone=apis burst=10;
    proxy_pass https://${XIVOCC_HOST};

    include /etc/nginx/xivo/proxy_params;
  }

```

## Call qualification JSON configuration

You can find some explanations for the json properties in man.config.json.
The most difficult part of the configuration is the x, y, cols and rows properties. Here is a description :

- "cols" is the number of column the element will take
- "rows" is the number of rows the element will take
- "x" is the starting column of the element
- "y" is the starting row of the element

In the grid system, "x" and "y" start at zero, if you want your first element to start at the top left you need to set "x" and "y" at zero.
Then, if you want to place another element next to it (two elements on the same line), you need to set the second element "x" property
equal to your previous element "cols" number so they don't overlap. To set an element on the line below, you need to set it's "y"
property equal to your previous element "rows" number so they don't overlap. You can leave gap inbetween elements if you want to but
you should not make them overlap each other.

Here is an example of volume mount for the configuration:
- /etc/docker/call-qualification/assets:/usr/share/nginx/html/assets

Here is an example of the configuration (default.config.json):

```bash
{
  "title": "qualification",
  "isCustom": false,
  "qualificationIndex": 2,
  "timeoutAfter": -1,
  "timeoutText": " ",
  "background": "#ffffff",
  "cardBackground": "#ffffff",
  "fields": [
    {
      "lines": [
        {
          "title": "assets/logo.png",
          "fieldType": 4,
          "subTitle": "center"
        }
      ],
      "cols": 4,
      "rows": 2,
      "x": 0,
      "y": 0
    },
    {
      "lines": [
        {
          "title": "Motif - Niveau 1",
          "fieldType": 0,
          "subTitle": "Motif - Niveau 2",
          "subFieldType": 0,
          "isMandatory": true,
          "formControlName": "qualification",
          "subFormControlName": "subQualification"
        }
      ],
      "isQualification": true,
      "cols": 4,
      "rows": 1,
      "x": 0,
      "y": 2
    },
    {
      "lines": [
        {
          "title": "Commentaire",
          "fieldType": 1,
          "formControlName": "comment"
        }
      ],
      "cols": 3,
      "rows": 1,
      "x": 0,
      "y": 3
    },
    {
      "lines": [
        {
          "title": "Agent",
          "fieldType": 0,
          "fieldValue": "callee",
          "readOnly": true,
          "formControlName": "callee"
        }
      ],
      "cols": 1,
      "rows": 1,
      "x": 3,
      "y": 3
    },
    {
      "lines": [
        {
          "title": "boutons",
          "fieldType": 3,
          "fieldValue": true
        }
      ],
      "cols": 4,
      "rows": 1,
      "x": 0,
      "y": 4
    }
  ]
}
```

## Logo

You can set a custom logo at the top of the qualification, in the same assets as the JSON configuration. It has to be named logo.png.

## Additional infos

The input containing `"fieldValue": "callee"` in the configuration is mandatory for the qualification to work, you must keep
it, otherwise it will prevent the data to be read and saved in base.
